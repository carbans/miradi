from django.conf.urls import url
from projects.views import home_page


urlpatterns = [
    url(r'^$', home_page, name='home'),
]
